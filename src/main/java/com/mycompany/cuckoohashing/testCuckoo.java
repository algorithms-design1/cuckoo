/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cuckoohashing;

/**
 *
 * @author PC Sakda
 */
public class testCuckoo {
    public static void main(String[] args) {
        Node cuckoo = new Node();
        cuckoo.put(20, "A");
        System.out.println(Node.T0[9].getValue()); // key 20 % 11 = 9      table1[index=9] value = A
        cuckoo.put(50, "B");
        System.out.println(Node.T0[6].getValue());// key 50 % 11 = 6      table1[index=6] value = B

        // กรณีเกิดการชน
        System.out.println("in the event of a collision");
        cuckoo.put(53, "C");
        System.out.println(Node.T0[9].getValue());// key 53 % 11 = 9      table1[index=9] value = C ไล่ตัวก่อนหน้าคือ A ไปที่ table2
        System.out.println(Node.T1[1].getValue());// key (20/11) % 11 = 1      table2[index=1] value = A
        
        System.out.println("in the event of a collision 2");
        cuckoo.put(75, "D");
        System.out.println(Node.T0[9].getValue());// key 75 % 11 = 9      table1[index=9] value = D ไล่ตัวก่อนหน้าคือ C ไปที่ table2
        System.out.println(Node.T1[4].getValue());// key (53/11) % 11 = 4      table2[index=4] value = C 
        cuckoo.put(100, "E");
        System.out.println(Node.T0[1].getValue());// key 100 % 11 = 1   table1[index=1] value = E
        cuckoo.put(67, "F");
        System.out.println(Node.T0[1].getValue());// key 67 % 11 = 1      table1[index=1] value = F ไล่ตัวก่อนหน้าคือ E ไปที่ table2
        System.out.println(Node.T1[9].getValue());// key (100/11) % 11 = 9      table2[index=9] value = E 
        cuckoo.put(105, "G");
        System.out.println(Node.T0[6].getValue());// key 105 % 11 = 6      table1[index=6] value = G ไล่ตัวก่อนหน้าคือ B ไปที่ table2
        System.out.println(Node.T1[4].getValue()); // value B key (50/11) % 11 = 4 table2[index=4] value = B ไล่ตัวที่อยู่ก่อนหน้าคือ C ไปที่ table1
        System.out.println(Node.T0[9].getValue()); // value C key 53 % 11 = 4 table1[index=9] value = C
        

        System.out.println("test remove");
        System.out.println(cuckoo.get(20).getValue());
        cuckoo.remove(20);
        System.out.println(cuckoo.get(20));
    }
}
