/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cuckoohashing;

/**
 *
 * @author informatics
 */
public class Node {

    private int key;
    private String value;
    static Node[] T0 = new Node[11];
    static Node[] T1 = new Node[11];

    public Node() {

    }

    public Node(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int h0(int key) {
        return key % T0.length;
    }

    public int h1(int key) {
        return (key / 11) % T1.length;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public Node get(int key) {
        if (T0[h0(key)] != null && T0[h0(key)].key == key) {
            return T0[h0(key)];
        }
        if (T1[h1(key)] != null && T1[h1(key)].key == key) {
            return T1[h1(key)];
        }
        return null;
    }

    public Node remove(int key) {
        if (T0[h0(key)] != null && T0[h0(key)].key == key) {
            Node temp = T0[h0(key)];
            T0[h0(key)] = null;
            return temp;
        }
        if (T1[h1(key)] != null && T1[h1(key)].key == key) {
            Node temp = T1[h1(key)];
            T1[h1(key)] = null;
            return temp;
        }
        return null;
    }

    public void put(int key, String value) {

        if (T0[h0(key)] != null && T0[h0(key)].key == key) {
            T0[h0(key)].key = key;
            T0[h0(key)].value = value;
            return;
        }
        if (T1[h1(key)] != null && T1[h1(key)].key == key) {
            T1[h1(key)].key = key;
            T1[h1(key)].value = value;
            return;
        }
        int i = 0;

        while (true) {
            if (i == 0) {
                if (T0[h0(key)] == null) {
                    T0[h0(key)] = new Node();
                    T0[h0(key)].key = key;
                    T0[h0(key)].value = value;
                    return;
                }
            } else {
                if (T1[h1(key)] == null) {
                    T1[h1(key)] = new Node();
                    T1[h1(key)].key = key;
                    T1[h1(key)].value = value;
                    return;
                }
            }

            if (i == 0) {
                Node temp = T0[h0(key)];
                String tempValue = T0[h0(key)].value;
                int tempKey = T0[h0(key)].key;
                T0[h0(key)].key = key;
                T0[h0(key)].value = value;
                key = tempKey;
                value = tempValue;

            } else {
                String tempValue = T1[h1(key)].value;
                int tempKey = T1[h1(key)].key;
                Node temp = T1[h1(key)];
                T1[h1(key)].key = key;
                T1[h1(key)].value = value;
                key = tempKey;
                value = tempValue;
            }
            i = (i + 1) % 2;
        }
    }

}
